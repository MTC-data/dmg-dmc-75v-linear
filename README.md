# DMG DMC 75V Linear

This repository contains MTC data for the DMG DMC 75V Linear machine tool. 

As the displacement data was recorded using a 3-channel interferometer IDS 3010, it only contains data about the X-axis - EXX, EBX, & ECX (named according to ISO 230).

The temperature data was recorded using 10 different temperature sensors an different locations on and around the axis. 
The measurement frequency for the temperature sensors is slower than the displacement measurement, 
so the data is linearly interpolated between the supporting points.

## Data structure

Input data columns:

- position
- A Z-Axis
- C Air New
- M Table Front
- M Table Side
- A Rotation Table
- M Bellow X
- C X-Axis Air
- A Machine door
- C Scale X-Axis
- M Scale Y-Axis Case

Outputs:

Suffix abbe: Abbe error virtually corrected by .7m in -z direction to compensate measurement position relative to encoder position

- Raw Interferometer data:

    - EX0
    - EX1
    - EX2
    
- Raw Error data

    - EBX_rawest
    - ECX_rawest
    - EXX_rawest
    - EXX_rawest_abbe

- Error data corrected to E*(x=0)=0

    - EXX
    - EXX_abbe
    - EBX
    - ECX

